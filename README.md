# corona-info

ein kleines CLI tool um die wichtigsten Corona-Infos zu bekommen.

## Verwendung:
das Programm kann 3 Parameter entgegennehmen. Der erste ist das Kürzel des Bundestates, der zweite der Name der Stadt und der dritte die gewünschte Sprache.  
Das Programm kann entweder ohne Parameter, mit den ersten beiden oder mit allen Parametern aufgerufen werden.

## Quelle der Daten:
Die Daten kommen vom RKI, jedoch über den umweg rki-covid-api.now.sh. Für die Korrecktheit der Daten kann ich nicht garantieren, ich gehe aber von ihr aus.  
Die API die ich benutze hat ihr eigenes GitHub repository https://github.com/jgehrcke/covid-19-germany-gae, wird betrieben von Jan-Philip Gehrcke und steht unter der MIT Lizens.
