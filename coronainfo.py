#! /usr/bin/python3

import os
import sys
import requests
import json
from colorama import Fore

class kreis:    # german local district
    api = "https://rki-covid-api.now.sh/api/districts"
    kreis_liste = ['Flensburg', 'Kiel', 'Lübeck', 'Neumünster', 'Dithmarschen', 'Herzogtum Lauenburg', 'Nordfriesland', \
            'Ostholstein', 'Pinneberg', 'Plön', 'Rendsburg-Eckernförde', 'Schleswig-Flensburg', 'Segeberg', 'Steinburg', \
            'Stormarn', 'Hamburg', 'Braunschweig', 'Salzgitter', 'Wolfsburg', 'Gifhorn', 'Goslar', 'Helmstedt', 'Northeim', \
            'Peine', 'Wolfenbüttel', 'Göttingen', 'Region Hannover', 'Diepholz', 'Hameln-Pyrmont', 'Hildesheim', 'Holzminden', \
            'Nienburg (Weser)', 'Schaumburg', 'Celle', 'Cuxhaven', 'Harburg', 'Lüchow-Dannenberg', 'Lüneburg', 'Osterholz', \
            'Rotenburg (Wümme)', 'Heidekreis', 'Stade', 'Uelzen', 'Verden', 'Delmenhorst', 'Emden', 'Oldenburg (Oldb)', \
            'Osnabrück', 'Wilhelmshaven', 'Ammerland', 'Aurich', 'Cloppenburg', 'Emsland', 'Friesland', 'Grafschaft Bentheim', \
            'Leer', 'Oldenburg', 'Osnabrück', 'Vechta', 'Wesermarsch', 'Wittmund', 'Bremen', 'Bremerhaven', 'Düsseldorf', \
            'Duisburg', 'Essen', 'Krefeld', 'Mönchengladbach', 'Mülheim an der Ruhr', 'Oberhausen', 'Remscheid', 'Solingen', \
            'Wuppertal', 'Kleve', 'Mettmann', 'Rhein-Kreis Neuss', 'Viersen', 'Wesel', 'Bonn', 'Köln', 'Leverkusen', \
            'Städteregion Aachen', 'Düren', 'Rhein-Erft-Kreis', 'Euskirchen', 'Heinsberg', 'Oberbergischer Kreis', \
            'Rheinisch-Bergischer Kreis', 'Rhein-Sieg-Kreis', 'Bottrop', 'Gelsenkirchen', 'Münster', 'Borken', 'Coesfeld', \
            'Recklinghausen', 'Steinfurt', 'Warendorf', 'Bielefeld', 'Gütersloh', 'Herford', 'Höxter', 'Lippe', 'Minden-Lübbecke', \
            'Paderborn', 'Bochum', 'Dortmund', 'Hagen', 'Hamm', 'Herne', 'Ennepe-Ruhr-Kreis', 'Hochsauerlandkreis', \
            'Märkischer Kreis', 'Olpe', 'Siegen-Wittgenstein', 'Soest', 'Unna', 'Darmstadt', 'Frankfurt am Main', \
            'Offenbach am Main', 'Wiesbaden', 'Bergstraße', 'Darmstadt-Dieburg', 'Groß-Gerau', 'Hochtaunuskreis', \
            'Main-Kinzig-Kreis', 'Main-Taunus-Kreis', 'Odenwaldkreis', 'Offenbach', 'Rheingau-Taunus-Kreis', 'Wetteraukreis', \
            'Gießen', 'Lahn-Dill-Kreis', 'Limburg-Weilburg', 'Marburg-Biedenkopf', 'Vogelsbergkreis', 'Kassel', 'Fulda', \
            'Hersfeld-Rotenburg', 'Kassel', 'Schwalm-Eder-Kreis', 'Waldeck-Frankenberg', 'Werra-Meißner-Kreis', 'Koblenz', \
            'Ahrweiler', 'Altenkirchen (Westerwald)', 'Bad Kreuznach', 'Birkenfeld', 'Cochem-Zell', 'Mayen-Koblenz', 'Neuwied', \
            'Rhein-Hunsrück-Kreis', 'Rhein-Lahn-Kreis', 'Westerwaldkreis', 'Trier', 'Bernkastel-Wittlich', 'Eifelkreis Bitburg-Prüm', \
            'Vulkaneifel', 'Trier-Saarburg', 'Frankenthal (Pfalz)', 'Kaiserslautern', 'Landau in der Pfalz', 'Ludwigshafen am Rhein', \
            'Mainz', 'Neustadt an der Weinstraße', 'Pirmasens', 'Speyer', 'Worms', 'Zweibrücken', 'Alzey-Worms', 'Bad Dürkheim', \
            'Donnersbergkreis', 'Germersheim', 'Kaiserslautern', 'Kusel', 'Südliche Weinstraße', 'Rhein-Pfalz-Kreis', 'Mainz-Bingen', \
            'Südwestpfalz', 'Stuttgart', 'Böblingen', 'Esslingen', 'Göppingen', 'Ludwigsburg', 'Rems-Murr-Kreis', 'Heilbronn', \
            'Heilbronn', 'Hohenlohekreis', 'Schwäbisch Hall', 'Main-Tauber-Kreis', 'Heidenheim', 'Ostalbkreis', 'Baden-Baden', \
            'Karlsruhe', 'Karlsruhe', 'Rastatt', 'Heidelberg', 'Mannheim', 'Neckar-Odenwald-Kreis', 'Rhein-Neckar-Kreis', 'Pforzheim', \
            'Calw', 'Enzkreis', 'Freudenstadt', 'Freiburg im Breisgau', 'Breisgau-Hochschwarzwald', 'Emmendingen', 'Ortenaukreis', \
            'Rottweil', 'Schwarzwald-Baar-Kreis', 'Tuttlingen', 'Konstanz', 'Lörrach', 'Waldshut', 'Reutlingen', 'Tübingen', \
            'Zollernalbkreis', 'Ulm', 'Alb-Donau-Kreis', 'Biberach', 'Bodenseekreis', 'Ravensburg', 'Sigmaringen', 'Ingolstadt', \
            'München', 'Rosenheim', 'Altötting', 'Berchtesgadener Land', 'Bad Tölz-Wolfratshausen', 'Dachau', 'Ebersberg', 'Eichstätt', \
            'Erding', 'Freising', 'Fürstenfeldbruck', 'Garmisch-Partenkirchen', 'Landsberg am Lech', 'Miesbach', 'Mühldorf a. Inn', \
            'München', 'Neuburg-Schrobenhausen', 'Pfaffenhofen a.d. Ilm', 'Rosenheim', 'Starnberg', 'Traunstein', 'Weilheim-Schongau', \
            'Landshut', 'Passau', 'Straubing', 'Deggendorf', 'Freyung-Grafenau', 'Kelheim', 'Landshut', 'Passau', 'Regen', 'Rottal-Inn', \
            'Straubing-Bogen', 'Dingolfing-Landau', 'Amberg', 'Regensburg', 'Weiden i.d. OPf.', 'Amberg-Sulzbach', 'Cham', \
            'Neumarkt i.d. OPf.', 'Neustadt a.d. Waldnaab', 'Regensburg', 'Schwandorf', 'Tirschenreuth', 'Bamberg', 'Bayreuth', \
            'Coburg', 'Hof', 'Bamberg', 'Bayreuth', 'Coburg', 'Forchheim', 'Hof', 'Kronach', 'Kulmbach', 'Lichtenfels', \
            'Wunsiedel i. Fichtelgebirge', 'Ansbach', 'Erlangen', 'Fürth', 'Nürnberg', 'Schwabach', 'Ansbach', 'Erlangen-Höchstadt', \
            'Fürth', 'Nürnberger Land', 'Neustadt a.d. Aisch-Bad Windsheim', 'Roth', 'Weißenburg-Gunzenhausen', 'Aschaffenburg', \
            'Schweinfurt', 'Würzburg', 'Aschaffenburg', 'Bad Kissingen', 'Rhön-Grabfeld', 'Haßberge', 'Kitzingen', 'Miltenberg', \
            'Main-Spessart', 'Schweinfurt', 'Würzburg', 'Augsburg', 'Kaufbeuren', 'Kempten (Allgäu)', 'Memmingen', 'Aichach-Friedberg', \
            'Augsburg', 'Dillingen a.d. Donau', 'Günzburg', 'Neu-Ulm', 'Lindau (Bodensee)', 'Ostallgäu', 'Unterallgäu', 'Donau-Ries', \
            'Oberallgäu', 'Regionalverband Saarbrücken', 'Merzig-Wadern', 'Neunkirchen', 'Saarlouis', 'Saarpfalz-Kreis', 'St. Wendel', \
            'Brandenburg an der Havel', 'Cottbus', 'Frankfurt (Oder)', 'Potsdam', 'Barnim', 'Dahme-Spreewald', 'Elbe-Elster', \
            'Havelland', 'Märkisch-Oderland', 'Oberhavel', 'Oberspreewald-Lausitz', 'Oder-Spree', 'Ostprignitz-Ruppin', \
            'Potsdam-Mittelmark', 'Prignitz', 'Spree-Neiße', 'Teltow-Fläming', 'Uckermark', 'Rostock', 'Schwerin', \
            'Mecklenburgische Seenplatte', 'Rostock', 'Vorpommern-Rügen', 'Nordwestmecklenburg', 'Vorpommern-Greifswald', \
            'Ludwigslust-Parchim', 'Chemnitz', 'Erzgebirgskreis', 'Mittelsachsen', 'Vogtlandkreis', 'Zwickau', 'Dresden', 'Bautzen', \
            'Görlitz', 'Meißen', 'Sächsische Schweiz-Osterzgebirge', 'Leipzig', 'Leipzig', 'Nordsachsen', 'Dessau-Roßlau', \
            'Halle (Saale)', 'Magdeburg', 'Altmarkkreis Salzwedel', 'Anhalt-Bitterfeld', 'Börde', 'Burgenlandkreis', 'Harz', \
            'Jerichower Land', 'Mansfeld-Südharz', 'Saalekreis', 'Salzlandkreis', 'Stendal', 'Wittenberg', 'Erfurt', 'Gera', 'Jena', \
            'Suhl', 'Weimar', 'Eisenach', 'Eichsfeld', 'Nordhausen', 'Wartburgkreis', 'Unstrut-Hainich-Kreis', 'Kyffhäuserkreis', \
            'Schmalkalden-Meiningen', 'Gotha', 'Sömmerda', 'Hildburghausen', 'Ilm-Kreis', 'Weimarer Land', 'Sonneberg', \
            'Saalfeld-Rudolstadt', 'Saale-Holzland-Kreis', 'Saale-Orla-Kreis', 'Greiz', 'Altenburger Land', 'Berlin Reinickendorf', \
            'Berlin Charlottenburg-Wilmersdorf', 'Berlin Treptow-Köpenick', 'Berlin Pankow', 'Berlin Neukölln', 'Berlin Lichtenberg', \
            'Berlin Marzahn-Hellersdorf', 'Berlin Spandau', 'Berlin Steglitz-Zehlendorf', 'Berlin Mitte', \
            'Berlin Friedrichshain-Kreuzberg', 'Berlin Tempelhof-Schöneberg'] 
    def __init__(self, name="",k_id=0):
        self.kreis_name = name
        self.kreis_id = k_id
        if name != "":
            self.kreis_id = self.kreis_liste.index(name)
    def refresh(self):
        req = requests.get(self.api)
        if req.status_code == 200:
            self.raw_json = json.loads(req.content)
            self.refresh = self.raw_json['lastUpdate']
            self.kreis_json = self.raw_json['districts'][self.kreis_id]
        else:
            print ("[!] Erro: unsuspected HTTP_Status Code " + str(req.status_code))
    def get(self, item_list=['weekIncidence', 'count', 'casesPer100k', 'deaths']):
        result = []
        for i in item_list:
            result.append(self.kreis_json[i])
        result.append(self.refresh)
        return result

class state:
    api = "https://rki-covid-api.now.sh/api/states"
    state_name_list = ['Schleswig-Holstein', 'Hamburg', 'Niedersachsen', 'Bremen', 'Nordrhein-Westfalen', 'Hessen', 'Rheinland-Pfalz', 'Baden-Württemberg', 'Bayern', 'Saarland', 'Berlin', 'Brandenburg', 'Mecklenburg-Vorpommern', 'Sachsen', 'Sachsen_Anhalt', 'Thüringen']
    def __init__(self, name):
        self.state_name = name
    def refresh(self):
        req = requests.get(self.api)
        if req.status_code == 200:
            self.raw_json = json.loads(req.content)
            self.refresh = self.raw_json['lastUpdate']
            self.state_json = self.raw_json['states'][self.state_name_list.index(self.state_name)]
        else:
            print ("[!] Erro: unsuspected HTTP_Status Code " + str(req.status_code))
    def get(self, item_list=['count', 'casesPer100k', 'deaths', 'weekIncidence']):
        result = []
        for i in item_list:
            result.append(self.state_json[i])
        result.append(self.refresh)
        return result

def get_rki_local_data(k_name, s_name):
    kr = kreis(name=k_name)
    kr.refresh()
    k_out = kr.get()
    st = state(s_name)
    st.refresh()
    s_out = st.get()

    fm = 0
    for i in st.raw_json['states']:
        fm += i['casesPer100k']
    fm = fm / 16

    fmI = 0 
    for i in kr.raw_json['districts']:
        fmI += i['weekIncidence']
    fmI = fmI / 412

    return s_out, k_out, fm, fmI

def output_de(s_dat, k_dat, sn, kn, fm, fmI):
    print ('DataLicense: dl-de/by-2-0 www.govdata.de/dl-de/by-2-0 Robert Koch Institut')
    print ("----------------Bundesland-----------------------")
    print ("Name:                       " + sn)
    print ("Fallzahlen:                 " + str(s_dat[0]))
    print ("Fälle auf 100k Einwohner:   " + str(s_dat[1])) 
    print ("Bundesdurchschnitt:         " + str(fm))
    print ("Todesfälle:                 " + str(s_dat[2]))
    print ("Inzidenz:                   " + str(s_dat[3]))
    print ("Letzte Aktualisierung:      " + str(s_dat[4]))    
    print ("-------------------------------------------------")   
    print ("")    
    print ("----------------Kreis----------------------------")   
    print ("Name:                       " + str(kn))
    if k_dat[0] >=50:
        print (Fore.RED + "Inzidenz:                   " + str(k_dat[0]))  
    elif k_dat[0] >= 35:
        print (Fore.YELLOW + "Inzidenz:                   " + str(k_dat[0]))
    elif k_dat[0] >= 20:
        print (Fore.GREEN + "Inzidenz:                   " + str(k_dat[0]))
    else:
        print (Fore.WHITE + "Inzidenz:                   " + str(k_dat[0]))
    print (Fore.WHITE + "Bundesdurchschnitt Inzidenz:" + str(fmI))
    print ("Fälle:                      " + str(k_dat[1]))
    print ("Fälle auf 100k Einwohner:   " + str(k_dat[2]))
    print ("Todesfälle:                 " + str(k_dat[3]))
    print ("Letzte Aktualisierung:      " + str(k_dat[4]))
    print ("-------------------------------------------------")

def output_en(s_dat, k_dat, sn, kn, fm, fmI):
    print ('DataLicense: dl-de/by-2-0 www.govdata.de/dl-de/by-2-0 Robert Koch Institut')
    print ("----------------State----------------------------")
    print ("name:                       " + sn)
    print ("Cases:                      " + str(s_dat[0]))
    print ("Cases per 100k citicen:     " + str(s_dat[1]))
    print ("Federal midel:              " + str(fm))
    print ("Deaths:                     " + str(s_dat[2]))
    print ("Incidence:                  " + str(s_dat[3]))
    print ("Last refresh:               " + str(s_dat[4]))    
    print ("-------------------------------------------------")   
    print ("")    
    print ("----------------Kreis----------------------------")   
    print ("name:                       " + str(kn))  
    if k_dat[0] >=50:
        print (Fore.RED + "Incidence:                  " + str(k_dat[0]))  
    elif k_dat[0] >= 35:
        print (Fore.YELLOW + "Incidence:                  " + str(k_dat[0]))
    elif k_dat[0] >= 20:
        print (Fore.GREEN + "Incidence:                  " + str(k_dat[0]))
    else:
        print (Fore.WHITE + "Incidence:                  " + str(k_dat[0]))
    print (Fore.WHITE + "Federal midel Incidence:    " + str(fmI))
    print ("Cases:                      " + str(k_dat[1]))
    print ("Cases per 100k citicen:     " + str(k_dat[2]))
    print ("Deaths:                     " + str(k_dat[3]))
    print ("Last refresh:               " + str(k_dat[4]))
    print ("-------------------------------------------------")

if sys.argv[0] == 'coronainfo.py':
    if len(sys.argv) == 4:
        lang = sys.argv[3]
    else:
        lang = 'en'
    if len(sys.argv) >= 2:
        st = sys.argv[1]
        kr = sys.argv[2]
    else:
        kr = input ('Kreis / local distrikt > ')
        st = input ('Bundesland / State > ')
    d_state, d_kreis, federal_middel, federal_middel_incidence = get_rki_local_data(kr, st)
    if lang == "de":
        output_de(d_state, d_kreis, st, kr, federal_middel, federal_middel_incidence)
    else:
        output_en(d_state, d_kreis, st, kr, federal_middel, federal_middel_incidence)
